// --- Day 2: Rock Paper Scissors ---
//
// The Elves begin to set up camp on the beach. To decide whose tent gets to be
// closest to the snack storage, a giant Rock Paper Scissors tournament is already
// in progress.
//
// Rock Paper Scissors is a game between two players. Each game contains many
// rounds; in each round, the players each simultaneously choose one of Rock,
// Paper, or Scissors using a hand shape. Then, a winner for that round is
// selected: Rock defeats Scissors, Scissors defeats Paper, and Paper defeats
// Rock. If both players choose the same shape, the round instead ends in a draw.
//
// Appreciative of your help yesterday, one Elf gives you an encrypted strategy
// guide (your puzzle input) that they say will be sure to help you win. "The
// first column is what your opponent is going to play: A for Rock, B for Paper,
// and C for Scissors. The second column--" Suddenly, the Elf is called away
// to help with someone's tent.
//
// The second column, you reason, must be what you should play in response: X
// for Rock, Y for Paper, and Z for Scissors. Winning every time would be
// suspicious, so the responses must have been carefully chosen.
//
// The winner of the whole tournament is the player with the highest score. Your
// total score is the sum of your scores for each round. The score for a single
// round is the score for the shape you selected (1 for Rock, 2 for Paper, and
// 3 for Scissors) plus the score for the outcome of the round (0 if you lost,
// 3 if the round was a draw, and 6 if you won).
//
// Since you can't be sure if the Elf is trying to help you or trick you, you
// should calculate the score you would get if you were to follow the strategy
// guide.
//
// For example, suppose you were given the following strategy guide:
//
// A Y
// B X
// C Z
//
// This strategy guide predicts and recommends the following:
//
//     In the first round, your opponent will choose Rock (A), and you should
//     choose Paper (Y). This ends in a win for you with a score of 8
//     (2 because you chose Paper + 6 because you won).
//
//     In the second round, your opponent will choose Paper (B), and you should
//     choose Rock (X). This ends in a loss for you with a score of 1 (1 + 0).
//
//     The third round is a draw with both players choosing Scissors, giving
//     you a score of 3 + 3 = 6.
//
// In this example, if you were to follow the strategy guide, you would get a
// total score of 15 (8 + 1 + 6).
//
// What would your total score be if everything goes exactly according to your
// strategy guide?
//
// Answer: 13052
//
// --- Part Two ---
//
// The Elf finishes helping with the tent and sneaks back over to you. "Anyway,
// the second column says how the round needs to end: X means you need to lose,
// Y means you need to end the round in a draw, and Z means you need to win.
// Good luck!"
//
// The total score is still calculated in the same way, but now you need to figure
// out what shape to choose so the round ends as indicated. The example above
// now goes like this:
//
//     In the first round, your opponent will choose Rock (A), and you need the
//     round to end in a draw (Y), so you also choose Rock. This gives you a
//     score of 1 + 3 = 4.
//
//     In the second round, your opponent will choose Paper (B), and you choose
//     Rock so you lose (X) with a score of 1 + 0 = 1.
//
//     In the third round, you will defeat your opponent's Scissors with Rock
//     for a score of 1 + 6 = 7.
//
// Now that you're correctly decrypting the ultra top secret strategy guide,
// you would get a total score of 12.
//
// Following the Elf's instructions for the second column, what would your total
// score be if everything goes exactly according to your strategy guide?
//
// Answer: 13693

const std = @import("std");
const fs = std.fs;

const example_data = @embedFile("example.txt");
const puzzle_data = @embedFile("input.txt");

const Answer = struct {
    naive_score: u32,
    strategy_score: u32,
};

// {{{ find answer

const Outcome = enum(u8) { Lose = 0, Draw = 3, Win = 6 };
const Choice = enum(u8) { Rock = 1, Paper = 2, Scissors = 3 };

// outcome score + choice score
const SCORE_TABLE = [9]u8{
    @intFromEnum(Outcome.Draw) + @intFromEnum(Choice.Rock), // A X
    @intFromEnum(Outcome.Win) + @intFromEnum(Choice.Paper), // A Y
    @intFromEnum(Outcome.Lose) + @intFromEnum(Choice.Scissors), // A Z
    @intFromEnum(Outcome.Lose) + @intFromEnum(Choice.Rock), // B X
    @intFromEnum(Outcome.Draw) + @intFromEnum(Choice.Paper), // B Y
    @intFromEnum(Outcome.Win) + @intFromEnum(Choice.Scissors), // B Z
    @intFromEnum(Outcome.Win) + @intFromEnum(Choice.Rock), // C X
    @intFromEnum(Outcome.Lose) + @intFromEnum(Choice.Paper), // C Y
    @intFromEnum(Outcome.Draw) + @intFromEnum(Choice.Scissors), // C Z
};

const STRATEGY_TABLE = [9]u8{
    @intFromEnum(Outcome.Lose) + @intFromEnum(Choice.Scissors), // lose vs rock
    @intFromEnum(Outcome.Draw) + @intFromEnum(Choice.Rock), // draw vs rock
    @intFromEnum(Outcome.Win) + @intFromEnum(Choice.Paper), // win vs rock
    @intFromEnum(Outcome.Lose) + @intFromEnum(Choice.Rock), // lose vs paper
    @intFromEnum(Outcome.Draw) + @intFromEnum(Choice.Paper), // draw vs paper
    @intFromEnum(Outcome.Win) + @intFromEnum(Choice.Scissors), // win vs paper
    @intFromEnum(Outcome.Lose) + @intFromEnum(Choice.Paper), // lose vs scissors
    @intFromEnum(Outcome.Draw) + @intFromEnum(Choice.Scissors), // draw vs scissors
    @intFromEnum(Outcome.Win) + @intFromEnum(Choice.Rock), // win vs scissors
};

fn get_answer_from_string(
    data: []const u8,
) !Answer {
    var lines = std.mem.split(u8, data, "\n");
    var naive_score: u32 = 0;
    var strategy_score: u32 = 0;

    while (lines.next()) |line| {
        if (line.len == 0) break;

        const i: u8 = line[0] - 'A';
        const j: u8 = line[2] - 'X';

        naive_score += SCORE_TABLE[3 * i + j];
        strategy_score += STRATEGY_TABLE[3 * i + j];
    }

    return Answer{ .naive_score = naive_score, .strategy_score = strategy_score };
}

// }}}

pub fn main() void {
    std.debug.print("Day 2!\n\n", .{});

    // {{{ Example

    const example = get_answer_from_string(example_data) catch {
        std.debug.print("Could not determine answer.", .{});
        return;
    };

    std.debug.print(
        "1. Your example answer was {d} ({d}).\n",
        .{ example.naive_score, 15 },
    );
    std.debug.print(
        "2. Your example answer was {d} ({d}).\n",
        .{ example.strategy_score, 12 },
    );

    // }}}

    // {{{ Puzzle

    const puzzle = get_answer_from_string(puzzle_data) catch {
        std.debug.print("Could not determine answer.", .{});
        return;
    };

    std.debug.print(
        "1. Your puzzle answer was {d} ({d}).\n",
        .{ puzzle.naive_score, 13052 },
    );
    std.debug.print(
        "2. Your puzzle answer was {d} ({d}).\n",
        .{ puzzle.strategy_score, 13693 },
    );

    // }}}
}
